# Prav Private Beta Release Party

These are the posters for the private beta release party. These were designed with
Penpot, and you can access the original Penpot project [here](https://pp.vern.cc/#/view/e4deb7d9-90a8-8155-8001-eda093ec1547?page-id=4755a563-5b43-8015-8001-e60d684cdbe6&section=interactions&index=0&share-id=e4deb7d9-90a8-8155-8001-f16e6cea1c28).

Note that for the SVGs to render correctly you will need to have the following
fonts installed:

  - English
    - [Jost](https://indestructibletype.com/Jost.html)
    - Playfair Display

  - Tamil
    - Baloo Thambi 2
    - Arimaa Madurai

  - Hindi
    - Baloo 2
    - Yatra One
