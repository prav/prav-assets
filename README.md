# Prav Assets

This repo is for all the logos, posters, and other assets of the Prav project.

# Credits

 - Beta Release Party posters
    - Badri Sunderarajan ([@badrihippo](https://fosstodon.org/@badrihippo))
    - Flying pigeon graphic by [wildchief](https://openclipart.org/artist/wildchief) on [Openclipart](https://openclipart.org/detail/84853/a-flying-pigeon-by-wildchief)
    - Tamil translaton thanks to the [Villupuram GNU/Linux Users Group (VGLUG)](https://vglug.org/)
    - Hindi translation thanks to Vijay Kumar
  - Created with the following fonts
    - [Arimaa Madurai](https://www.ndiscover.com/arima/) ([NDISCOVER](https://www.ndiscover.com/), OFL)
    - [Baloo 2](https://ektype.in/font-family/baloo2.html) and [Baloo Thambi 2](https://ektype.in/font-family/baloo-thambi2.html) ([Ek Type](https://ektype.in/), OFL)
    - [Jost](https://indestructibletype.com/Jost.html) ([Indestructible Type](https://indestructibletype.com/Home.html), OFL)
    - [Playfair Display](https://github.com/clauseggers/Playfair) ([Claude Eggers](https://www.forthehearts.net/), OFL)
    - [Yatra One](https://github.com/cathschmidt/yatra-one) (Catherine Leigh Schmidt, OFL)

# License
![Creative Commons License](https://i.creativecommons.org/l/by-sa/3.0/88x31.png)
Assets are released under the [Creative Commons Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/). Credit is to be given to the Prav project with a link back to this repo.
