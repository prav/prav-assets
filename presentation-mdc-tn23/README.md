# Presentation for the [TN MiniDebConf 2023](https://tn23.mini.debconf.org/)

These are the files for the Prav presentation by [Badri](https://fosstodon.org/@badrihippo) at
the MiniDebConf in Villupuram in January 2023.

  - Presentation.penpot: the original Penpot file
  - Presentation.svg: SVG render of the Penpot file
  - Presentation.pdf: PDF render of the Penpot file

You can also see the file online [here](https://design.penpot.app/#/view/2a21f5ae-60ee-8151-8001-f474e0c74999?page-id=e4deb7d9-90a8-8155-8001-f46ee2fafb27&section=interactions&index=0&share-id=4755a563-5b43-8015-8001-fced1a0f11e6&zoom=fit).

Font used in this file:

  - Baloo 2
