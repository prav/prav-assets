## Credits 

The brochure was created by [Moksh](https://codeberg.org/moksh) using LibreOffice and was adapted from [Debutsav's brochure](https://drive.google.com/file/d/1a_9n1-KkVyQvR9DKvCbvp69vXwnIYqBk/view?usp=sharing). If you would like to adapt this file to your needs, you can edit the .odt file by using [LibreOffice](https://libreoffice.org) and export as PDF from there. The files are licensed under the [CC-BY-SA 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/).
